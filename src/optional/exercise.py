"""Hints:

* ``Optional[SomeType]`` is equivalent to ``Union[SomeType, None]``

Docs:

* mypy
<https://mypy.readthedocs.io/en/latest/kinds_of_types.html#optional-types-and-the-none-type>
"""


def within_datetime_range(start, target, end):
    return (start is None or start <= target) and (end is None or target < end)
