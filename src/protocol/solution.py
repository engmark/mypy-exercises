from abc import abstractmethod
from typing import Dict, TypeVar

from typing_extensions import Protocol, runtime_checkable

Key = TypeVar("Key")  # pragma: no mutate
Value = TypeVar("Value")  # pragma: no mutate


@runtime_checkable
class Invertible(Protocol[Key, Value]):
    @abstractmethod
    def inverse(self) -> "Invertible[Value, Key]":
        raise NotImplementedError


class InvertibleDict(Dict[Key, Value]):
    def inverse(self) -> "InvertibleDict[Value, Key]":
        new_instance: "InvertibleDict[Value, Key]" = (  # pragma: no mutate
            self.__class__()
        )
        for key, value in self.items():
            new_instance[value] = key
        return new_instance
