from copy import deepcopy
from sys import version_info

from pytest import raises
from pytest_subtests import SubTests

from ..generators import any_string
from .exercise import InvertibleDict as Exercise
from .solution import Invertible
from .solution import InvertibleDict as Solution


def test_should_handle_inversion(subtests: SubTests) -> None:
    key = any_string()
    value = any_string()

    for class_ in (Exercise, Solution):
        dictionary = class_({key: value})

        with subtests.test(
            msg=f"{class_.__module__} should allow lookup of value in inverse"
        ):
            assert dictionary.inverse()[value] == key

        with subtests.test(
            msg=f"{class_.__module__} should correspond to the Invertible protocol"
        ):
            assert isinstance(dictionary, Invertible)

        with subtests.test(
            msg=f"{class_.__module__}.inverse should not modify the original dictionary"
        ):
            dictionary_copy = deepcopy(dictionary)
            dictionary.inverse()
            assert dictionary_copy == dictionary

        with subtests.test(
            msg=f"{class_.__module__} should get same dictionary after double inversion"
        ):
            assert dictionary.inverse().inverse() == dictionary


def test_solution_code_should_raise_not_implemented_error_for_abstract_method() -> None:
    with raises(NotImplementedError):
        Invertible.inverse(Solution())


def test_solution_code_should_not_allow_instantiating_protocol() -> None:
    prefix = "Can't instantiate abstract class Invertible"
    error_message_regex = (
        f"^{prefix} without an implementation for abstract method 'inverse'$"
        if version_info >= (3, 12)  # pragma: no cover
        else f"^{prefix} with abstract method inverse$"
    )

    with raises(TypeError, match=error_message_regex):
        Invertible()  # type: ignore[misc]
