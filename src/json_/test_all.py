from pytest import raises
from pytest_subtests import SubTests

from ..generators import any_json, any_json_dict, any_string
from .exercise import OverlappingJSONError
from .exercise import strict_merge_json_dicts as exercise
from .solution import strict_merge_json_dicts as solution


def test_should_merge_non_overlapping_json_dicts(subtests: SubTests) -> None:
    first_key = any_string()
    first_value = any_json()

    while (second_key := any_string()) == first_key:
        pass  # pragma: no cover (only runs in case of a match)
    second_value = any_json()

    for function in (exercise, solution):
        with subtests.test(msg=function.__module__):
            assert function({first_key: first_value}, {second_key: second_value}) == {
                first_key: first_value,
                second_key: second_value,
            }


def test_should_throw_exception_for_overlapping_json_dicts(subtests: SubTests) -> None:
    duplicate_key = any_string()
    first = {**any_json_dict(), **{duplicate_key: any_json()}}
    second = {**any_json_dict(), **{duplicate_key: any_json()}}

    for function in (exercise, solution):
        with (
            subtests.test(msg=function.__module__),
            raises(OverlappingJSONError) as error,
        ):
            function(first, second)
        assert duplicate_key in error.value.args[0]
