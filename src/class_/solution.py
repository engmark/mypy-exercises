from dataclasses import dataclass
from socket import gethostname
from typing import Optional, Type


@dataclass  # pragma: no mutate
class ModelBase:
    hostname: str


def model_with_meta(hostname: Optional[str] = None) -> Type[ModelBase]:
    actual_hostname = hostname if hostname else gethostname()

    @dataclass  # pragma: no mutate
    class Model(ModelBase):
        hostname = actual_hostname

    return Model
