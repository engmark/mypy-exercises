from pytest import raises
from pytest_subtests import SubTests

from .exercise import chain_exceptions as exercise
from .solution import chain_exceptions as solution


class InnerError(Exception):
    pass


class OuterError(Exception):
    pass


def test_should_throw_given_exception(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=function.__module__), raises(OuterError):
            function(InnerError(), OuterError())
