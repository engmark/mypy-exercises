"""Docs:

* mypy <https://mypy.readthedocs.io/en/latest/more_types.html#the-noreturn-type>"""


def chain_exceptions(inner, outer):
    raise outer from inner
