from pytest_subtests import SubTests

from . import exercise, solution


def test_should_not_treat_non_positive_integers_as_positive(subtests: SubTests) -> None:
    for package in (exercise, solution):
        for value in (-2, -1, 0):
            with subtests.test(msg=f"{package.__name__} {value}"):
                assert not package.is_positive(value)


def test_should_treat_positive_integers_as_such(subtests: SubTests) -> None:
    for package in (exercise, solution):
        for value in (1,):
            with subtests.test(msg=f"{package.__name__} {value}"):
                assert package.is_positive(value)


def test_should_not_treat_odd_integers_as_even(subtests: SubTests) -> None:
    for package in (exercise, solution):
        for value in (-1, 1):
            with subtests.test(msg=f"{package.__name__} {value}"):
                assert not package.is_even(value)


def test_should_not_treat_even_integers_as_such(subtests: SubTests) -> None:
    for package in (exercise, solution):
        for value in (2,):
            with subtests.test(msg=f"{package.__name__} {value}"):
                assert package.is_even(value)


def test_should_not_treat_zero_or_odd_positive_integers_as_even_positive_integers(
    subtests: SubTests,
) -> None:
    for package in (exercise, solution):
        for value in (0, 1):
            with subtests.test(msg=f"{package.__name__} {value}"):
                assert not package.is_even_positive_integer(value)


def test_should_treat_even_positive_integers_as_such(
    subtests: SubTests,
) -> None:
    for package in (exercise, solution):
        for value in (2, 4):
            with subtests.test(msg=f"{package.__name__} {value}"):
                assert package.is_even_positive_integer(value)
