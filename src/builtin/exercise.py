"""Hints:

* Builtin types are always available, even without importing them.
* To annotate a variable, parameter or field use ``CONTAINER: TYPE``, for example
  ``name: str``.
* To annotate a function or method use ``def FUNCTION(PARAMETERS) -> TYPE``, for example
``def name() -> str``.

Docs:

* Python standard library <https://docs.python.org/3/library/stdtypes.html>
* mypy <https://mypy.readthedocs.io/en/latest/builtin_types.html>"""


def format_error_message(severity, code, message):
    return f"{severity} {code}: '{message}'"
