from dataclasses import dataclass

from typing_extensions import TypedDict


class PersonTypeDef(TypedDict):
    name: str


@dataclass  # pragma: no mutate
class Person:
    def __init__(self, name: str):
        self.name = name

    def to_dict(self) -> PersonTypeDef:
        return {"name": self.name}


def person_from_dict(value: PersonTypeDef) -> Person:
    return Person(value["name"])
