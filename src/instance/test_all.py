"""If you haven't learned about ``TypedDict`` yet, just think of
``PersonDict(name=NAME) `` as ``{"name": NAME}``"""

from pytest_subtests import SubTests

from . import exercise, solution

NAME = "Jane Doe"
PERSON_AS_DICT = solution.PersonTypeDef(name=NAME)


def test_should_serialize_person(subtests: SubTests) -> None:
    for module in (exercise, solution):
        with subtests.test(msg=module.__name__):
            assert module.Person(NAME).to_dict() == PERSON_AS_DICT


def test_should_deserialize_person(subtests: SubTests) -> None:
    for module in (exercise, solution):
        person = module.person_from_dict(PERSON_AS_DICT)

        with subtests.test(msg=f"{module.__name__} type"):
            assert isinstance(person, module.Person)

        with subtests.test(msg=f"{module.__name__} name"):
            assert person.name == NAME
