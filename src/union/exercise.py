"""Hints:

* ``code`` can be either an integer or a string
* ``body`` is a string

Docs:

* mypy <https://mypy.readthedocs.io/en/latest/kinds_of_types.html#union-types>"""

STATUS_KEY = "status"
BODY_KEY = "body"


def create_response(code, body):
    return {STATUS_KEY: code, BODY_KEY: body}
