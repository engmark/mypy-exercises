from random import choice, randrange, uniform
from string import printable
from typing import Callable, List, Optional, Union

from .json_.solution import Json, JsonDict


def any_string() -> str:
    """Includes ASCII printable characters and the first printable character from
    several Unicode blocks
    <https://en.wikipedia.org/wiki/List_of_Unicode_characters>."""
    return "".join(
        choice(f"{printable}¡ĀƀḂəʰͰἀЀ–⁰₠℀⅐←∀⌀①─▀■☀🬀✁ㄅﬀ")
        for _ in range(randrange(0, stop=5))
    )


def any_integer() -> int:
    return randrange(-5, 5)


def any_positive_integer() -> int:
    return randrange(1, 5)


def any_non_zero_integer() -> int:
    return any_positive_integer() * choice((-1, 1))


def any_float() -> float:
    return uniform(-5.0, 5.0)  # pragma: no cover


def any_boolean() -> bool:
    return choice([True, False])  # pragma: no cover


def none() -> None:
    return None  # pragma: no cover


def any_json_primitive() -> Optional[Union[str, int, float, bool]]:  # pragma: no cover
    methods: List[Callable[[], Optional[Union[str, int, float, bool]]]] = [
        any_string,
        any_integer,
        any_float,
        any_boolean,
        none,
    ]
    return choice(methods)()


def any_json() -> Json:
    methods: List[Callable[[], Json]] = [
        any_string,
        any_integer,
        any_float,
        any_boolean,
        none,
        any_json_list,
        any_json_dict,
    ]
    return choice(methods)()


def any_json_list() -> List[Json]:  # pragma: no cover
    result = []
    for _ in range(randrange(0, 5)):
        result.append(any_json())

    return result


def any_json_dict() -> JsonDict:
    result = {}
    for _ in range(randrange(0, 5)):
        result[any_string()] = any_json()  # pragma: no cover

    return result
