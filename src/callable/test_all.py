from unittest.mock import MagicMock

from pytest_subtests import SubTests

from .exercise import curry as exercise
from .solution import curry as solution

CURRIED_ARGS = [1, 2]
CURRIED_KWARGS = {"foo": 3, "bar": 4}
NEW_ARGS = [5, 6]
NEW_KWARGS = {"baz": 7, "bay": 8}


def test_should_return_curried_function(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=function.__module__):
            original_function = MagicMock()
            curried_function = function(
                original_function, *CURRIED_ARGS, **CURRIED_KWARGS
            )

            curried_function(*NEW_ARGS, **NEW_KWARGS)

            original_function.assert_called_once_with(
                *CURRIED_ARGS, *NEW_ARGS, **CURRIED_KWARGS, **NEW_KWARGS
            )
