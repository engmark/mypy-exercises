from pytest_subtests import SubTests

from ..generators import any_non_zero_integer, any_positive_integer
from .exercise import product as exercise
from .solution import product as solution


def test_should_return_product_of_values(subtests: SubTests) -> None:
    factors = []
    product = 1
    for _ in range(any_positive_integer()):
        factor = any_non_zero_integer()
        factors.append(factor)
        product *= factor

    for function in (exercise, solution):
        with subtests.test(msg=f"{function.__module__}: {factors}"):
            assert function(factors) == product
