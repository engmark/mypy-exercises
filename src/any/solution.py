from typing import Any


def commutative_equals(first: Any, second: Any) -> bool:
    first_equals_second: bool = first == second
    second_equals_first: bool = second == first
    return first_equals_second and second_equals_first
