from datetime import datetime

from .typed_dicts import Extent, SpatialExtent, StacCollection, TemporalExtent


def test_should_create_dict() -> None:
    assert StacCollection(
        description="any description",
        extent=Extent(
            spatial=SpatialExtent(bbox=[[-180, -90, 180, 90]]),
            temporal=TemporalExtent(interval=[[datetime(2000, 1, 1), None]]),
        ),
        id="any ID",
        license="MIT",
        links=[],
        version="1.0.0",
        type="collection",
    ) == {
        "description": "any description",
        "extent": {
            "spatial": {"bbox": [[-180, -90, 180, 90]]},
            "temporal": {"interval": [[datetime(2000, 1, 1, 0, 0), None]]},
        },
        "id": "any ID",
        "license": "MIT",
        "links": [],
        "version": "1.0.0",
        "type": "collection",
    }
