"""Your IDE should show an indication of a problem in the last line. If you inspect it,
it should say something like this:

    "Expected type 'str', got '_Hash' instead"

(pyright) or

    Argument 1 to "ChecksumMismatchError" has incompatible type "_Hash"; expected "str"

(mypy)."""

from hashlib import sha256


class ChecksumMismatchError(Exception):
    def __init__(self, hex_digest: str):
        super().__init__()

        self.hex_digest = hex_digest


checksum = sha256(b"contents\n")
raise ChecksumMismatchError(checksum)
