from ..generators import any_string
from .complex_types import name
from .name_reuse import Person


def test_should_get_name_from_person() -> None:
    value = any_string()
    person = Person(value)
    assert name(person) == value


def test_should_get_name_from_string() -> None:
    value = any_string()
    assert name(value) == value


def test_should_get_name_from_mapping() -> None:
    value = any_string()
    assert name({"name": value}) == value
