"""Using the same name more than once for things with a different type is a common code
smell."""

from dataclasses import dataclass


@dataclass  # pragma: no mutate
class Person:
    def __init__(self, name: str):
        self.name = name


OWNER = Person("Jane Doe")
print(OWNER)
OWNER = "Jane Doe"
print(OWNER)
