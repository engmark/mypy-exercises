from inspect import stack
from typing import TYPE_CHECKING, Any, BinaryIO

if TYPE_CHECKING:
    from _typeshed import OpenBinaryMode
else:
    OpenBinaryMode = Any


def caller_file(mode: OpenBinaryMode) -> BinaryIO:
    caller_path = stack()[1].filename
    return open(caller_path, mode=mode)
