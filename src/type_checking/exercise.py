"""Hints:

* ``TYPE_CHECKING`` makes it easy to avoid installing stubs to run code in production.
* For anything imported `if TYPE_CHECKING`, make sure to create a corresponding runtime
object.

Docs:

* ``TYPE_CHECKING`` <https://docs.python.org/3/library/typing.html#typing.TYPE_CHECKING>
* mypy
<https://mypy.readthedocs.io/en/stable/runtime_troubles.html#typing-type-checking>
"""

from inspect import stack


def caller_file(mode):
    caller_path = stack()[1].filename
    return open(caller_path, mode=mode)
