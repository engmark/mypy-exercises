from pytest_subtests import SubTests

from .exercise import is_equality_method_sane as exercise
from .solution import is_equality_method_sane as solution


class ReferenceClass:
    def __eq__(self, other: object) -> bool:
        return self is other

    def __hash__(self) -> int:
        return super().__hash__()  # pragma: no cover


class AlwaysTrue:
    def __eq__(self, other: object) -> bool:
        return True

    def __hash__(self) -> int:
        return super().__hash__()  # pragma: no cover


class AlwaysFalse:
    def __eq__(self, other: object) -> bool:
        return False

    def __hash__(self) -> int:
        return super().__hash__()  # pragma: no cover


class Negated:
    def __eq__(self, other: object) -> bool:
        return not super().__eq__(other)

    def __hash__(self) -> int:
        return super().__hash__()  # pragma: no cover


class PropertyBased:
    def __init__(self, name: str):
        self.name = name

    def __eq__(self, other: object) -> bool:
        return isinstance(other, PropertyBased) and self.name == other.name

    def __hash__(self) -> int:
        return super().__hash__()  # pragma: no cover


def test_should_treat_reference_as_sane(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=function.__module__):
            assert function(ReferenceClass(), ReferenceClass()) is True


def test_should_treat_wrong_implementations_as_not_sane(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=function.__module__):
            for class_ in (AlwaysTrue, AlwaysFalse, Negated):
                with subtests.test(msg=class_.__name__):
                    assert function(class_(), class_()) is False

            with subtests.test(msg=PropertyBased.__name__):
                assert function(PropertyBased("foo"), PropertyBased("foo")) is False
