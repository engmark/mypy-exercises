from logging import DEBUG, NOTSET

from pytest import LogCaptureFixture
from pytest_subtests import SubTests

from ..generators import any_string
from . import exercise, solution


def test_should_log_message(caplog: LogCaptureFixture, subtests: SubTests) -> None:
    program = any_string()
    message = any_string()

    for module in (exercise, solution):
        with caplog.at_level(NOTSET), subtests.test(msg=module.__name__):
            caplog.clear()
            module.log_message(program, message)

            assert caplog.record_tuples == [
                (module.__name__, DEBUG, f"{program}: {message}")
            ]
