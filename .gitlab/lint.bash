#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

pre-commit run --all-files
.gitlab/verify-exercises.bash
gitlint --commits="origin/${CI_DEFAULT_BRANCH:?}..origin/${CI_COMMIT_REF_NAME:?}" --debug --fail-without-commits
