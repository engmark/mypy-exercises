{
  description = "mypy-exercises flake using uv2nix";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    pyproject-nix = {
      url = "github:nix-community/pyproject.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    uv2nix = {
      url = "github:adisbladis/uv2nix";
      inputs.pyproject-nix.follows = "pyproject-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      nixpkgs,
      uv2nix,
      pyproject-nix,
      ...
    }:
    let
      inherit (nixpkgs) lib;

      workspace = uv2nix.lib.workspace.loadWorkspace { workspaceRoot = ./.; };

      overlay = workspace.mkPyprojectOverlay {
        sourcePreference = "wheel";
      };

      pyprojectOverrides = _final: _prev: {
        # Implement build fixups here.
      };

      pkgs = nixpkgs.legacyPackages.x86_64-linux;

      python = pkgs.python3;

      pythonSet =
        (pkgs.callPackage pyproject-nix.build.packages {
          inherit python;
        }).overrideScope
          (lib.composeExtensions overlay pyprojectOverrides);

    in
    {
      packages.x86_64-linux.default = pythonSet.mkVirtualEnv "mypy-exercises-env" workspace.deps.default;

      devShells.x86_64-linux.default =
        let
          editableOverlay = workspace.mkEditablePyprojectOverlay {
            root = "$REPO_ROOT";
          };

          editablePythonSet = pythonSet.overrideScope editableOverlay;

          virtualenv = editablePythonSet.mkVirtualEnv "mypy-exercises-dev-env" workspace.deps.all;

        in
        pkgs.mkShell {
          env = {
            LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
          };
          packages = [
            virtualenv
            pkgs.bashInteractive
            pkgs.cacert
            pkgs.check-jsonschema
            pkgs.curl
            pkgs.deadnix
            pkgs.gitFull
            pkgs.gitlint
            pkgs.jq
            pkgs.mutmut
            pkgs.nixfmt-rfc-style
            pkgs.nodePackages.prettier
            pkgs.pre-commit
            pkgs.ruff
            pkgs.shellcheck
            pkgs.shfmt
            pkgs.statix
            pkgs.taplo
            pkgs.uv
            pkgs.yq-go
          ];
          shellHook = ''
            # Undo dependency propagation by nixpkgs.
            unset PYTHONPATH
            # Get repository root using git. This is expanded at runtime by the editable `.pth` machinery.
            export REPO_ROOT=$(git rev-parse --show-toplevel)
            ln --force --no-target-directory --symbolic "${virtualenv}/bin/python" python
          '';
        };
    };
}
